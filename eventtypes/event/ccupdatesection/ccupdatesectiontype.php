<?php
/**
 * @package ccUpdateSection
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    29 Apr 2017
 **/

class ccUpdateSectionType extends eZWorkflowEventType
{
	const TYPE_ID = 'ccupdatesection';

	public function __construct() {
		$this->eZWorkflowEventType( self::TYPE_ID, 'Updates current object section based on location and content type' );
		$this->setTriggerTypes(
			array(
				'content' => array(
					'publish' => array( 'after', 'before' )
				)
			)
		);
	}

	public function execute( $process, $event ) {
		$nodeIDs    = array();
		$parameters = $process->attribute( 'parameter_list' );

		$object = null;
		if( isset( $parameters['object_id'] ) ) {
			$object = eZContentObject::fetch( $parameters['object_id'] );
		}

		if( $object instanceof eZContentObject === false ) {
			return eZWorkflowType::STATUS_ACCEPTED;
		}

		if( (int) $object->attribute( 'current_version' ) !== 1 ) {
			return eZWorkflowType::STATUS_ACCEPTED;
		}

		$newSectionID = self::getSectionID( $object );
		if( $newSectionID > 0 && $newSectionID !== (int) $object->attribute( 'section_id' ) ) {
            $object->setAttribute( 'section_id', $newSectionID );
            $object->store();
		}

		return eZWorkflowType::STATUS_ACCEPTED;
	}

	public static function getSectionID( eZContentObject $object ) {
		$rules = self::getRules();

		$parentNodes = array();
		$assignments = eZNodeAssignment::fetchForObject(
			$object->attribute( 'id' ),
			$object->attribute( 'current_version' ),
			1
		);
		foreach( $assignments as $assignment ) {
			$node = $assignment->attribute( 'parent_node_obj' );
			if( $node instanceof eZContentObjectTreeNode ) {
				$parentNodes[] = $node;
			}
		}

		foreach( $rules as $rule ) {
			$matchClass = false;
			$matchNodes = false;

			if( count( $rule['content_classes'] ) === 0 ) {
				$matchClass = true;
			} else {
				$matchClass = in_array( $object->attribute( 'class_identifier' ), $rule['content_classes'] );
			}

			if( count( $rule['parent_node_ids'] ) === 0 ) {
				$matchNodes = true;
			} else {
				foreach( $parentNodes as $parentNode ) {
					foreach( $rule['parent_node_ids'] as $ruleNodeID ) {
						if( in_array( $ruleNodeID, $parentNode->attribute( 'path_array' ) ) ) {
							$matchNodes = true;
							break 2;
						}
					}
				}
			}

			if( $matchClass && $matchNodes ) {
				return $rule['section_id'];
			}
		}

		return null;
	}

	public static function getRules() {
		$rules = array();

		$ini = eZINI::instance( 'ccupdatesection.ini' );
		foreach( $ini->variable( 'General', 'Rules' ) as $ruleName ) {
			if( $ini->hasVariable( $ruleName, 'SectionIdentifier' ) === false ) {
				continue;
			}

			$section = eZSection::fetchByIdentifier( $ini->variable( $ruleName, 'SectionIdentifier' ) );
			if( $section instanceof eZSection === false ) {
				continue;
			}

			$rule = array(
				'section_id'      => (int) $section->attribute( 'id' ),
				'parent_node_ids' => $ini->hasVariable( $ruleName, 'ParentNodeIDs' ) ? $ini->variable( $ruleName, 'ParentNodeIDs' ) : array(),
				'content_classes' => $ini->hasVariable( $ruleName, 'ContentClasses' ) ? $ini->variable( $ruleName, 'ContentClasses' ) : array()
			);

			if( count( $rule['parent_node_ids'] ) === 0 && count( $rule['content_classes'] ) === 0 ) {
				continue;
			}

			$rules[] = $rule;
		}

		return $rules;
	}
}

eZWorkflowEventType::registerEventType( ccUpdateSectionType::TYPE_ID, 'ccUpdateSectionType' );
